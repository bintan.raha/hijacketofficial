---
title: Flowink Runnink Predator - RNK-PREDATOR
description: Jual jaket Flowink Runnink Predator - RNK-PREDATOR
date: '2018-07-12T17:48:14+07:00'
slug: rnk-predator
model:
  - runnink
brand:
  - flowink
thumbnail: /images/runnink/runnink-predator.jpg
image:
  - /images/runnink/runnink-predator-1.jpg
  - /images/runnink/runnink-predator-2.jpg
  - /images/runnink/runnink-predator-3.jpg
  - /images/runnink/runnink-predator-4.jpg
  - /images/runnink/runnink-predator-5.jpg
  - /images/runnink/runnink-predator-6.jpg
  - /images/runnink/runnink-predator-7.jpg
sku: RNK-PREDATOR
badge: ''
berat: 700 gram
layout: flowink
color:
  - Predator
size:
  - name: All Size
    price: 220000
  - name: XL
    price: 230000
stock: true
---

FLOWINK RUNNINK ORIGINAL Series, adventuring-inspired layer with a color-blocked design on lightweight fabric for an iconic look

- • 100 % Polyfiber
- • Imported
- • Machine Wash
- • 75% Waterproof & Windproof
- • Lightweight Fabric for iconic look
- • AE Nets Furing

#### Tabel Ukuran Jaket Flowink Runnink Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 108-110         | 112-114	      |
| Lingkar Lengan  | 46-48           | 48-50  	      |
| Panjang Tangan  | 60-62           | 62-64  	      |
| Panjang Badan   | 67-69           | 70-72  	      |
