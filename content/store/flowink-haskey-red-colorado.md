---
title: Flowink Haskey Red Colorado - HSK-COLORADO
description: Jual jaket Flowink Haskey Red Colorado - HSK-COLORADO
date: '2018-07-12T17:48:14+07:00'
slug: rnk-colorado
model:
  - haskey
brand:
  - flowink
thumbnail: /images/haskey/haskey-red-colorado.jpg
image:
  - /images/haskey/haskey-red-colorado-1.jpg
  - /images/haskey/haskey-red-colorado-2.jpg
  - /images/haskey/haskey-red-colorado-3.jpg
  - /images/haskey/haskey-red-colorado-4.jpg
  - /images/haskey/haskey-red-colorado-5.jpg
  - /images/haskey/haskey-red-colorado-6.jpg
  - /images/haskey/haskey-red-colorado-7.jpg
sku: HSK-COLORADO
badge: ''
berat: 700 gram
layout: flowink
color:
  - Red
size:
  - name: All Size
    price: 220000
  - name: XL
    price: 230000
stock: true
---

FLOWINK HASKEY ORIGINAL Series, adventuring-inspired layer with a color-blocked design on lightweight fabric for an iconic look

- • 100 % Polyfiber
- • Imported
- • Machine Wash
- • 75% Waterproof & Windproof
- • Lightweight Fabric for iconic look
- • AE Nets Furing

#### Tabel Ukuran Jaket Flowink Haskey Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 108-110         | 112-114	      |
| Lingkar Lengan  | 46-48           | 48-50  	      |
| Panjang Tangan  | 60-62           | 62-64  	      |
| Panjang Badan   | 67-69           | 70-72  	      |
