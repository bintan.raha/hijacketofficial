---
title: Hijacket Elma Cherry - HJ-EL
description: Jual jaket muslimah Hijacket Elma Cherry - HJ-EL
date: '2018-04-04T17:48:14+07:00'
slug: hj-el-cherry
product:
  - elma
brand:
  - hijacket
thumbnail: /images/elma/el-cherry.jpg
image:
  - /images/elma/el-cherry-1.jpg
  - /images/elma/el-cherry-2.jpg
  - /images/elma/el-cherry-3.jpg
  - /images/elma/el-cherry-4.jpg
  - /images/elma/el-cherry-5.jpg
sku: HJ-EL-CHERRY
badge: ''
berat: 700 gram
color:
  - Cherry
size:
  - name: All Size
    price: 185000
  - name: XL
    price: 195000
stock: true
---

HIJACKET  ELMA ORIGINAL merupakan  seri hijacket gaya modern yang tak lekang oleh waktu, hijacket elma dilengkapi dengan bentuk feminin, saku yang praktis dan pas serbaguna dan dengan tombol pearlescent vintage-chic. Sangat cocok untuk hijaber yang suka tampil formal dan berstyle jas.

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Bordir Berkualitas

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 4 variasi warna Hijacket Elma Original

#### Tabel Ukuran Hijacket Elma Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 89-90           | 89-90  	      |
