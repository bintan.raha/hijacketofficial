---
title: Hijacket Urbanashion Harbor - HJ-UB
description: Jual jaket muslimah Hijacket Urbanashion Harbor - HJ-UB
date: '2018-04-04T17:48:14+07:00'
slug: hj-ub-harbor
product:
  - urbanashion
brand:
  - hijacket
thumbnail: /images/urbanashion/ub-harbor.jpg
image:
  - /images/urbanashion/ub-harbor-1.jpg
  - /images/urbanashion/ub-harbor-2.jpg
  - /images/urbanashion/ub-harbor-3.jpg
  - /images/urbanashion/ub-harbor-4.jpg
  - /images/urbanashion/ub-harbor-5.jpg
  - /images/urbanashion/ub-harbor-6.jpg
sku: HJ-UB-HARBOR
badge: ''
berat: 700 gram
color:
  - Harbor
size:
  - name: All Size
    price: 180000
  - name: XL
    price: 190000
  - name: XXL
    price: 200000
stock: true
---

HIJACKET URBANASHION ORIGINAL model Hijacket ini didesain khas bergaya urban yang berkesan elegan bikin penampilan Anda tambah berwibawa. Cocok untuk wanita karir atau muslimah profesional. Dengan model modis, dipadu tulisan beberapa nama negara, kantong, dan tali kerut agar lebih stylish.

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XXL (XL Nambah 10.000 & XXL Nambah 20.000 Dari Harga All Size L)

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 11 variasi warna Hijacket Urbanashion Original

#### Tabel Ukuran Hijacket Urbanashion Original


| Ukuran          | All Size        | XL              | XXL             |
|:--------------- |:---------------:|:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      | 118-120         |
| Lingkar Lengan  | 40-42           | 43-45  	      | 46-48           |
| Panjang Tangan  | 55-57           | 55-57  	      | 55-57           |
| Panjang Badan   | 93-95           | 93-95  	      | 93-95           |
