---
title: Flowink Runnink Spidey - RNK-SPIDEY
description: Jual jaket Flowink Runnink Spidey - RNK-SPIDEY
date: '2018-07-12T17:48:14+07:00'
slug: rnk-spidey
model:
  - runnink
brand:
  - flowink
thumbnail: /images/runnink/runnink-spidey.jpg
image:
  - /images/runnink/runnink-spidey-1.jpg
  - /images/runnink/runnink-spidey-2.jpg
  - /images/runnink/runnink-spidey-3.jpg
  - /images/runnink/runnink-spidey-4.jpg
  - /images/runnink/runnink-spidey-5.jpg
  - /images/runnink/runnink-spidey-6.jpg
  - /images/runnink/runnink-spidey-7.jpg
sku: RNK-SPIDEY
badge: ''
berat: 700 gram
layout: flowink
color:
  - Spidey
size:
  - name: All Size
    price: 220000
  - name: XL
    price: 230000
stock: true
---

FLOWINK RUNNINK ORIGINAL Series, adventuring-inspired layer with a color-blocked design on lightweight fabric for an iconic look

- • 100 % Polyfiber
- • Imported
- • Machine Wash
- • 75% Waterproof & Windproof
- • Lightweight Fabric for iconic look
- • AE Nets Furing

#### Tabel Ukuran Jaket Flowink Runnink Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 108-110         | 112-114	      |
| Lingkar Lengan  | 46-48           | 48-50  	      |
| Panjang Tangan  | 60-62           | 62-64  	      |
| Panjang Badan   | 67-69           | 70-72  	      |
