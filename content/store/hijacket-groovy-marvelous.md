---
title: Hijacket Groovy Marvelous - HJ-GR
description: Jual jaket muslimah Hijacket Groovy Marvelous - HJ-GR
date: '2018-04-04T17:48:14+07:00'
slug: hj-gr-marvelous
product:
  - groovy
brand:
  - hijacket
thumbnail: /images/groovy/marvelous.jpg
image:
  - /images/groovy/marvelous-1.jpg
  - /images/groovy/marvelous-2.jpg
  - /images/groovy/marvelous-3.jpg
  - /images/groovy/marvelous-4.jpg
  - /images/groovy/marvelous-5.jpg
sku: HJ-GR-MARVELOUS
badge: ''
berat: 700 gram
color:
  - Marvelous
size:
  - name: All Size
    price: 175000
  - name: XL
    price: 185000
stock: true
---

HIJACKET GROOVY ORIGINAL yang dirancang Hi-Fashion dengan tampilan super modis. Seri GROOVY adalah seri EKSTRIM, yang memiliki 2 sisi berbeda. Semakin ekstrim, semakin diminati oleh Hijaber

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Printing Dada : Polyflex Berkualitas untuk Icon Brand Hijacket

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 3 variasi warna Hijacket Grovy Original

#### Tabel Ukuran Hijacket Groovy Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 93-95           | 93-95  	      |
