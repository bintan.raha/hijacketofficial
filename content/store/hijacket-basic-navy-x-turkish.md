---
title: Hijacket Basic Navy X Turkish - HJ-2
description: Jual jaket muslimah Hijacket Basic Navy X Turkish - HJ-2
date: '2018-04-04T17:48:14+07:00'
slug: hj-2
product:
  - basic
brand:
  - hijacket
thumbnail: /images/basic/basic-hj2.jpg
image:
  - /images/basic/basic-hj2-1.jpg
  - /images/basic/basic-hj2-2.jpg
  - /images/basic/basic-hj2-3.jpg
  - /images/basic/basic-hj2-4.jpg
  - /images/basic/basic-hj2-5.jpg
sku: HJ-2
badge: ''
berat: 700 gram
color:
  - Navy X Turkish
size:
  - name: All Size
    price: 155000
  - name: XL
    price: 165000
  - name: XXL
    price: 175000
stock: true
---

#### Hijacket Basic Original

HIJACKET BASIC ORIGINAL merupakan rancangan pertama dari seri Hijacket. Dibuat untuk para Hijaber, dengan style panjang, dan mengikuti trend eropa. Menjadi seri paling wajib kamu miliki.

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XXL (XL NAIK 10.000 & XXL NAIK 20.000 Dari Harga All Size L)

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Fleece adalah bahan Textile yang banyak digunakan untuk membuat Sweater, Jumper Dan Jaket Hoddie terutama Jaket Distro, berbahan dasar woll/kapas atau cotton yang bagian dalamnya terdapat bulu-bulu halus sehingga bersifat menyerap keringat.

Premium Fleece merupakan bahan fleece terbaik dengan kelebihan bahannya yang lebih tebal, serat luar yang besar dan bagian dalamnya yang berbulu halus. 

Premium Fleece memiliki tampilan yang mewah, warnanya yang tidak cepat kusam dan tidak panas walaupun bahannya tebal karena berbahan dasar Cotton di atas 70%.

#### Tabel Ukuran Hijacket Basic Original


| Ukuran          | All Size        | XL              | XXL             |
|:--------------- |:---------------:|:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      | 118-120         |
| Lingkar Lengan  | 40-42           | 43-45  	      | 46-48           |
| Panjang Tangan  | 55-57           | 55-57  	      | 55-57           |
| Panjang Badan   | 93-95           | 93-95  	      | 93-95           |
