---
title: Hijacket Montix Navy - HJ-MT
description: Jual jaket muslimah Hijacket Montix Navy - HJ-MT
date: '2018-04-04T17:48:14+07:00'
slug: hj-mt-navy
product:
  - montix
brand:
  - hijacket
thumbnail: /images/montix/montix-navy.jpg
image:
  - /images/montix/montix-navy-1.jpg
  - /images/montix/montix-navy-2.jpg
  - /images/montix/montix-navy-3.jpg
  - /images/montix/montix-navy-4.jpg
  - /images/montix/montix-navy-5.jpg
  - /images/montix/montix-navy-6.jpg
sku: HJ-MT-NAVY
badge: ''
berat: 700 gram
color:
  - Navy
size:
  - name: All Size
    price: 225000
  - name: XL
    price: 235000
stock: true
---

HIJACKET MONTIX ORIGINAL, adventuring-inspired layer for Hijaber with a color-blocked design on lightweight fabric for an iconic look 100% Polyfiber Imported, 75% Waterproof & Windproof, Full Dourmill Dacron, Pocket Inside.

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶️ Material : 100% Polyfiber Imported, 75% Waterproof & Windproof, Full Dourmill Dacron

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Sablonan Berkualitas + Pocket Inside

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 7 variasi warna Hijacket Montix Original

#### Tabel Ukuran Hijacket Montix Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 104-106         | 110-112	      |
| Lingkar Lengan  | 48-50           | 52-54  	      |
| Panjang Tangan  | 58-60           | 58-60  	      |
| Panjang Badan   | 83-85           | 85-87  	      |
