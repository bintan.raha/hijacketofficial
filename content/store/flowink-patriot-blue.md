---
title: Flowink Patriot Blue - (PT-BLUE)
description: Jual jaket Flowink Patriot Blue - (PT-BLUE)
date: '2018-07-12T17:48:14+07:00'
slug: rnk-blue
model:
  - patriotic
brand:
  - flowink
thumbnail: /images/patriotic/patriotic-blue.jpg
image:
  - /images/patriotic/patriotic-blue-1.jpg
  - /images/patriotic/patriotic-blue-2.jpg
  - /images/patriotic/patriotic-blue-3.jpg
sku: PT-BLUE
badge: ''
berat: 700 gram
layout: flowink
color:
  - Blue
size:
  - name: All Size
    price: 200000
stock: true
---

FLOWINK PATRIOT ORIGINAL Series mengekspresikan energi kecintaan Flowink terhadap Indonesia & kekuatan penggunanya dalam memimpin trend. Dengan printing berkualitas bertuliskan :

DON’T EVER GO WITH FLOW
BE THE FLOW!
FLOWINKDONESIA

- • Fabric: Body: 100% polyester. Hand : 80% cotton/20% polyester, Hood : 50% cotton/50% polyester.
- • Machine wash
- • Shown: Blue
- • Printed Art
- • Machine Wash

#### Tabel Ukuran Jaket Flowink Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 108-110         | 112-114	      |
| Lingkar Lengan  | 46-48           | 48-50  	      |
| Panjang Tangan  | 60-62           | 62-64  	      |
| Panjang Badan   | 67-69           | 70-72  	      |
