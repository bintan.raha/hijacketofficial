---
title: Hijacket Qadira Raven Grey - HJ-QD
description: Jual jaket muslimah Hijacket Qadira Raven Grey - HJ-QD
date: '2018-04-04T17:48:14+07:00'
slug: hj-qd-raven-grey
product:
  - qadira
brand:
  - hijacket
thumbnail: /images/qadira/raven-grey.jpg
image:
  - /images/qadira/raven-grey-1.jpg
  - /images/qadira/raven-grey-2.jpg
  - /images/qadira/raven-grey-3.jpg
  - /images/qadira/raven-grey-4.jpg
  - /images/qadira/raven-grey-5.jpg
sku: HJ-QD-RAVEN-GREY
badge: ''
berat: 700 gram
color:
  - Raven Grey
size:
  - name: All Size
    price: 200000
  - name: XL
    price: 210000
stock: true
---

Hijacket Qadira Original merupakan seri hijacket sporty terbaru dirancacng lebih Modis dilengkapi dengan Bordir berkualitas dengan Identitas Hijaber (HJ) dibagian dada ditambah kata motivasi _“Hijab Makes Me Complete Hijacket Makes Me Perfect”_ dan _“Strength and Dignity Hijaber Forever”_. Sangat cocok untuk menyempurnakan gaya hijabmu dengan design yang Fresh dan paduan warna yang menarik.

- ▶ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶ Sablonan Berkualitas

- ▶ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 4 variasi warna Hijacket Qadira Original, pilih style favorit ukhti❤

#### Tabel Ukuran Hijacket Qadira Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 84-85           | 86-87  	      |
