---
title: Hijacket Rose Black - HJ-R&B
description: Jual jaket muslimah Hijacket Rose Black - HJ-R&B
date: '2018-04-04T17:48:14+07:00'
slug: hj-r-black
product:
  - rose butterfly
brand:
  - hijacket
thumbnail: /images/rose/rose-black.jpg
image:
  - /images/rose/rose-black-1.jpg
  - /images/rose/rose-black-2.jpg
  - /images/rose/rose-black-3.jpg
  - /images/rose/rose-black-4.jpg
  - /images/rose/rose-black-5.jpg
sku: HJ-R-BLACK
badge: ''
berat: 700 gram
color:
  - Black
size:
  - name: All Size
    price: 195000
stock: true
---

#### Hijacket Rose & Butterfly Original

HIJACKET R&B (ROSE & BUTTERFLY) adalah seri khusus buat hijaber karir & tampil megah. Dengan print Polyflex berkualitas dan paduan warna mewah menyatu dalam seri Rose & Butterfly Series ini

- ▶️ Ukuran : ALL SIZE FIT TO L

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Printing Dada : Polyflex Berkualitas untuk Icon Brand Hijacket

- ▶️ Custom Rib : (Karet Lengan & Leher) spesial hanya untuk seri ini

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 4 variasi warna Hijacket Rose & Butterfly Original

#### Tabel Ukuran Hijacket Rose & Butterfly Original


| Ukuran          | All Size        |
|:--------------- |:---------------:|
| Lingkar Dada    | 101-102         |
| Lingkar Lengan  | 40-42           |
| Panjang Tangan  | 55-57           |
| Panjang Badan   | 93-95           |
