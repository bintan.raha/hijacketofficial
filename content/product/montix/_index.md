---
title: Hijacket Montix Original
description: "Hijacket Montix Original di design khusus untuk hijaber yang suka ADVENTUR atau TURING dengan Material 100% Polyfiber Imported, 75% Waterproof & Windproof, Full Dourmill Dacron." 
image: "/images/hijacket-montix-cover.png"
slug: "montix"
---

Hijacket Montix Original di design khusus untuk hijaber yang suka ADVENTUR atau TURING dengan Material 100% Polyfiber Imported, 75% Waterproof & Windproof, Full Dourmill Dacron. Yuk Koleksi Sekrang Juga !