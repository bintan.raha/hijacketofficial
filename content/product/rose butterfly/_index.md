---
title: Hijacket Rose Butterfly Original
description: "Hijacket Rose Butterfly Original adalah seri khusus buat hijaber karir & tampil megah. Dengan print Polyflex berkualitas dan paduan warna mewah menyatu dalam seri Rose & Butterfly Series ini. Yuk koleksi hijacket rose & butterfly sekarang !" 
image: "/images/hijacket-rose-cover.png"
slug: "rose"
---

Hijacket Rose Butterfly Original adalah seri khusus buat hijaber karir & tampil megah. Dengan print Polyflex berkualitas dan paduan warna mewah menyatu dalam seri Rose & Butterfly Series ini. Yuk koleksi hijacket rose & butterfly sekarang!