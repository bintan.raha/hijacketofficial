---
title: "Tentang Kami"
description: "tentang kami"
slug: "tentang-kami"
layout: "tentang-kami"
---

Kami adalah partner resmi dari Brand Hijacket dan Flowink yang memasarkan berbagai varian produk jaket Hijacket dan Flowink secara eceran maupun grosir.

Produk yang kami jual ini dijamin asli dan original. Kami selalu berusaha memberikan produk berkualitas dengan harga bersaing dan pelayanan terbaik untuk Anda.

Kami berlokasi di Bandung dan siap melayani pengiriman ke seluruh Indonesia.

Untuk informasi lebih lengkap silahkan hubungi Customer Service kami.

Terima Kasih